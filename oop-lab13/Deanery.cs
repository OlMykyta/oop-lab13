﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Deanery
    {
        public Deanery()
        {
            Dean = new Teacher();
        }
        public Deanery(Teacher dean)
        {
            Dean = dean;
        }
        public Deanery(Deanery deanery)
        {
            Dean = deanery.Dean;
        }

        public Teacher Dean { protected set; get; }
    }
}