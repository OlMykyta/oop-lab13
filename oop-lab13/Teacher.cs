﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Teacher : Human
    {
        public Subject Subject { protected set; get; }

        public Department Department { protected set; get; }

        public Teacher() : base()
        {
            Subject = new Subject();
            Department = new Department();
        }
        public Teacher(string name, string secondname, string surname, Subject subject, Department department)
            :base(name, secondname, surname)
        {
            Subject = subject;
            Department = department;
        }
        public Teacher(Teacher teacher) : base(teacher)
        {
            Subject = teacher.Subject;
            Department = teacher.Department;
        }
    }
}