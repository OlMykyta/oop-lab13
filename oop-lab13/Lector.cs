﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Lector : Teacher
    {
        public Lector() : base() { }
        public Lector(string name, string secondname, string surname, Subject subject, Department department)
            :base(name, secondname, surname, subject, department) { }
        public Lector(Teacher teacher) : base(teacher) { }
        public Lector(Lector lector) : base(lector) { }

        public void SetProgresstoDep()
        {
            Department.SetProgress(new Progress());
        }
    }
}