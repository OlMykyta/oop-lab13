﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Marks
    {
        public Marks()
        {
            Labs = Practics = Lectures = Exam = Test = 0;
        }
        public Marks(int labs, int practics, int lectures, int exam, int test)
        {
            Labs = labs;
            Practics = practics;
            Lectures = lectures;
            Exam = exam;
            Test = test;
        }
        public Marks(Marks marks)
        {
            Labs = marks.Labs;
            Practics = marks.Practics;
            Lectures = marks.Lectures;
            Exam = marks.Exam;
            Test = marks.Test;
        }

        public int Labs { protected set; get; }

        public int Practics { protected set; get; }

        public int Lectures { protected set; get; }

        public int Exam { protected set; get; }

        public int Test { protected set; get; }
    }
}