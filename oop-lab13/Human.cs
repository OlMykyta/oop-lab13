﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Human
    {
        public Human()
        {
            Name = "None";
            SecondName = "None";
            Surname = "None";
        }

        public Human(string name, string secondname, string surname)
        {
            Name = name;
            SecondName = secondname;
            Surname = surname;
        }
        public Human(Human human)
        {
            Name = human.Name;
            SecondName = human.SecondName;
            Surname = human.Surname;
        }

        public string Name { protected set; get; }

        public string SecondName { protected set; get; }

        public string Surname { protected set; get; }
    }
}