﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Subject
    {
        public Subject()
        {
            Name = "None";
            Lectures = null;
            Practicals = null;
            LabWorks = null;
            Test = new Test();
            Exam = new Exam();
            Lector = new Lector();
            LabPracticalTeachers = null;
        }
        public Subject(string name, Lecture[] lectures, Practical[] practicals, LabWork[] labWorks, 
            Test test, Exam exam, Lector lector, Teacher[] labPractTeachers)
        {
            Name = name;
            Lectures = lectures;
            Practicals = practicals;
            LabWorks = labWorks;
            Test = test;
            Exam = exam;
            Lector = lector;
            LabPracticalTeachers = labPractTeachers;
        }
        public Subject(Subject subject)
        {
            Name = subject.Name;
            Lectures = subject.Lectures;
            Practicals = subject.Practicals;
            Test = subject.Test;
            Exam = subject.Exam;
            Lector = subject.Lector;
            LabPracticalTeachers = subject.LabPracticalTeachers;
        }

        public string Name { protected set; get; }

        public Lecture[] Lectures { protected set; get; }

        public Practical[] Practicals { protected set; get; }

        public LabWork[] LabWorks { protected set; get; }

        public Test Test { protected set; get; }

        public Exam Exam { protected set; get; }

        public Lector Lector { protected set; get; }

        public Teacher[] LabPracticalTeachers { protected set; get; }
    }
}