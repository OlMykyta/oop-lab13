﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Exam
    {
        public Exam()
        {
            Mark = 0;
        }
        public Exam(int mark)
        {
            Mark = mark;
        }
        public Exam(Exam exam)
        {
            Mark = exam.Mark;
        }
        public int Mark { protected set; get; }
    }
}