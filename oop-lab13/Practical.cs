﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Practical
    {
        public Practical()
        {
            Number = Mark = 0;
        }
        public Practical(int number, int mark)
        {
            Number = number;
            Mark = mark;
        }
        public Practical(Practical practical)
        {
            Number = practical.Number;
            Mark = practical.Mark;
        }

        public int Number { protected set; get; }

        public int Mark { protected set; get; }
    }
}