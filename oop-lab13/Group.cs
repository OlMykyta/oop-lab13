﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Group
    {
        public oop_lab13.Student[] Students { protected set; get; }

        public string GroupName { protected set; get; }
        public Group()
        {
            Students = null;
            GroupName = "None";
        }
        public Group(Student[] students, string name)
        {
            Students = students;
            GroupName = name;
        }
        public Group(Group group)
        {
            Students = group.Students;
            GroupName = group.GroupName;
        }
    }
}