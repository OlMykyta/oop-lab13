﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Department
    {
        public string Name { protected set; get; }
        public Subject[] Subjects { protected set; get; }
        public Progress Progress { protected set; get; }

        public Department()
        {
            Name = "None";
            Subjects = null;
        }
        public Department(string name, Subject[] subjects)
        {
            Name = name;
            Subjects = subjects;
        }
        public Department(Department department)
        {
            Name = department.Name;
            Subjects = department.Subjects;
        }

        public void SetProgress(Progress progress)
        {
            Progress = progress;
        }
    }
}