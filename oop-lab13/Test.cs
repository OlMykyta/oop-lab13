﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Test
    {
        public Test()
        {
            Mark = 0;
        }
        public Test(int mark)
        {
            Mark = mark;
        }
        public Test(Test test)
        {
            Mark = test.Mark;
        }
        public int Mark { protected set; get; }
    }
}