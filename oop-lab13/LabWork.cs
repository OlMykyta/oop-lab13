﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class LabWork
    {
        public LabWork()
        {
            Number = Mark = 0;
        }
        public LabWork(int number, int mark)
        {
            Number = number;
            Mark = mark;
        }
        public LabWork(LabWork labWork)
        {
            Number = labWork.Number;
            Mark = labWork.Mark;
        }

        public int Number { protected set; get; }

        public int Mark { protected set; get; }
    }
}