﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class StreamTeachers
    {
        public oop_lab13.Teacher[] Teachers { protected set; get; }

        public Subject Subject { protected set; get; }

        public Lector Lector { protected set; get; }
        public StreamTeachers()
        {
            Teachers = null;
            Subject = new Subject();
            Lector = new Lector();
        }
        public StreamTeachers(Teacher[] teachers, Subject subject, Lector lector)
        {
            Teachers = teachers;
            Subject = subject;
            Lector = lector;
        }
        public StreamTeachers(StreamTeachers stream)
        {
            Teachers = stream.Teachers;
            Subject = stream.Subject;
            Lector = stream.Lector;
        }
    }
}