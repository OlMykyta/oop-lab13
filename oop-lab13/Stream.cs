﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Stream
    {
        public Group[] Groups { protected set; get; }

        public int Course { protected set; get; }

        public StreamTeachers[] StreamTeachersArr { protected set; get; }
        public Stream()
        {
            Groups = null;
            Course = 0;
            StreamTeachersArr = null;
        }
        public Stream(Group[] groups, int course, StreamTeachers[] streamTeachers)
        {
            Groups = groups;
            Course = course;
            StreamTeachersArr = streamTeachers;
        }
        public Stream(Stream stream)
        {
            Groups = stream.Groups;
            Course = stream.Course;
            StreamTeachersArr = stream.StreamTeachersArr;
        }
    }
}