﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Student : Human
    {
        public Student() : base()
        {
            MarksArr = null;
        }
        public Student(string name, string secondname, string surname) : base(name, secondname, surname)
        {
            MarksArr = null;
        }
        public Student(string name, string secondname, string surname, Marks marks) 
            : base(name, secondname, surname)
        {
            MarksArr = marks;
        }
        public Student(Student student) :base(student)
        {
            MarksArr = student.MarksArr;
        }

        public oop_lab13.Marks[] MarksArr { protected set; get; }
    }
}