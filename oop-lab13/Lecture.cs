﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oop_lab13
{
    public class Lecture
    {
        public Lecture()
        {
            Number = Mark = 0;
            Lector = new Lector();
        }
        public Lecture(int number, int mark, Lector lector)
        {
            Number = number;
            Mark = mark;
            Lector = lector;
        }
        public Lecture(Lecture lecture)
        {
            Number = lecture.Number;
            Mark = lecture.Mark;
            Lector = lecture.Lector;
        }

        public int Number { protected set; get; }

        public int Mark { protected set; get; }

        public Lector Lector { protected set; get; }
    }
}